package com.epam.model;


import java.io.IOException;
import java.util.List;
import java.util.Set;

/**
 * Interface for Model.
 * Created by Borys Latyk on 13/11/2019.
 *
 * @version 2.1
 * @since 10.11.2019
 */
public interface Model {
    List<String> lines() throws IOException;
    List<String> sentencesLenght() throws IOException;
    List<String> separatedWords() throws IOException;
    List<String> uniqueWord() throws IOException;
    Set<String> questionSentences() throws IOException;
    List<String> changeWords() throws IOException;
    List<String> sortAlphabetically() throws IOException;
    List<String> consonantalSorting() throws IOException;
    List<String> deleteWords() throws IOException;
    List<String> cutWords() throws IOException;

}
