package com.epam.model;

import com.epam.controller.ControllerIml;

import java.io.*;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Class contains all the main logic of the application
 * Created by Borys Latyk on 01/12/2019.
 *
 * @version 2.1
 * @since 01.12.2019
 */
public class StringLinesLogic implements Model, FillList {
    static int count;

    List<String> lines = new ArrayList<>();
    List<String> sentences;
    List<String> words = new ArrayList<>();
    List<String> uniguesentences = new ArrayList<>();
    List<String> definedsentences = new ArrayList<>();
    List<String> uniqwords = new ArrayList<>();
    Set<String> definedwords = new TreeSet<>();

    FileInputStream fileInputStream;
    BufferedReader input = new BufferedReader
            (new InputStreamReader(System.in));

    ControllerIml controllerAG;


    public StringLinesLogic() {

    }

    public StringLinesLogic(ControllerIml controllerIml) {
        this.controllerAG = controllerIml;
    }

    @Override
    public List<String> seperateSentences() throws IOException {

        lines = new ArrayList<>();
        fileInputStream = new FileInputStream("E:/Epam knowledge/modifystrings/SourceFile.txt");
        input = new BufferedReader(new InputStreamReader(fileInputStream));
        String str;

        while ((str = input.readLine()) != null) {
            lines.add(str);
        }
        input.close();
        sentences = new ArrayList<>();
        for (String temp : lines) {
            Matcher matcher = Pattern.compile("([^.!?]+[.!?])").matcher(temp);
            while (matcher.find()) {
                sentences.add(matcher.group(1));
            }
        }

        return sentences;
    }

    @Override
    public List<String> separatedWords() throws IOException {
        StringLinesLogic w = new StringLinesLogic();
        for (String temp : w.seperateSentences()) {
            String a = temp;
            Pattern pattern =
                    Pattern.compile("\\w+", Pattern.UNICODE_CHARACTER_CLASS
                            | Pattern.CASE_INSENSITIVE);
            Matcher matcher1 = pattern.matcher(a);

            while (matcher1.find())
                words.add(matcher1.group().toLowerCase());
        }
        return words;
    }

    @Override
    public List<String> lines() throws IOException {
        StringLinesLogic s = new StringLinesLogic();

        for (String temp : s.seperateSentences()) {
            String a = temp;
            words.clear();
            Pattern pattern =
                    Pattern.compile("\\w+", Pattern.UNICODE_CHARACTER_CLASS
                            | Pattern.CASE_INSENSITIVE);
            Matcher matcher1 = pattern.matcher(a);

            while (matcher1.find())
                words.add(matcher1.group().toLowerCase());

            for (int i = words.size() - 1; i >= 0; i--) {
                count = 0;
                for (String res : words) {
                    if (words.get(i).equals(res)) {
                        count++;
                    }
                    if (count == 2 && !(res.equals("the") && !(res.equals("a") &&
                            !(res.equals("of")))) && !(uniguesentences.contains(a))) {
                        uniguesentences.add(a);
                        break;
                    }
                }
            }
        }
        return uniguesentences;
    }

    @Override
    public List<String> sentencesLenght() throws IOException {
        StringLinesLogic l = new StringLinesLogic();
        Comparator<String> fromSmall = Comparator.comparing(String::length);
        Collections.sort(l.seperateSentences(), fromSmall);
        return l.sentences;
    }

    @Override
    public List<String> uniqueWord() throws IOException {
        StringLinesLogic u = new StringLinesLogic();
        for (int i = 1; i < u.seperateSentences().size() - 1; i++) {
            definedsentences.add(u.seperateSentences().get(i));
        }
        for (String word : definedsentences) {
            String a = word;
            Pattern pattern =
                    Pattern.compile("\\w+", Pattern.UNICODE_CHARACTER_CLASS
                            | Pattern.CASE_INSENSITIVE);
            Matcher matcher1 = pattern.matcher(a);

            while (matcher1.find())
                words.add(matcher1.group().toLowerCase());

        }
        String sentence = u.seperateSentences().get(0);
        String[] wordsfromFirstSentence = sentence.split(" ");
        List<String> temp = Arrays.asList(wordsfromFirstSentence);
        for (int i = temp.size() - 1; i >= 0; i--) {
            if ((words.contains(temp.get(i).toLowerCase()) == false &&
                    !(temp.get(i).equalsIgnoreCase(".")))) {
                uniqwords.add(temp.get(i));

            }
        }
        return uniqwords;
    }

    @Override
    public Set<String> questionSentences() throws IOException {
        StringLinesLogic q = new StringLinesLogic();
        for (String question : q.seperateSentences()) {
            if (question.contains("?")) {
                String a = question;
                Pattern pattern =
                        Pattern.compile("\\w+", Pattern.UNICODE_CHARACTER_CLASS
                                | Pattern.CASE_INSENSITIVE);
                Matcher matcher1 = pattern.matcher(a);

                while (matcher1.find())
                    definedwords.add(matcher1.group().toLowerCase());
            }
        }

        return definedwords;
    }

    @Override
    public List<String> changeWords() throws IOException {
        StringLinesLogic c = new StringLinesLogic();
        for (String sentence : c.seperateSentences()) {
            String loudletter = "";
            String longestletter = "";
            List<String> tempsentence = Arrays.asList(sentence.split(" "));
            for (int i = 0; i < tempsentence.size() - 1; i++) {
                if (tempsentence.get(i).toLowerCase().startsWith("a") ||
                        tempsentence.get(i).toLowerCase().startsWith("e") ||
                        tempsentence.get(i).toLowerCase().startsWith("i") ||
                        tempsentence.get(i).toLowerCase().startsWith("o") ||
                        tempsentence.get(i).toLowerCase().startsWith("u")) {

                    loudletter = tempsentence.get(i);
                    break;
                }
            }
            for (String word : tempsentence) {
                if (word.length() > longestletter.length()) {
                    longestletter = word;
                }
            }
            String modified = sentence.replaceAll(loudletter, longestletter);
            definedsentences.add(modified);
        }
        return definedsentences;
    }

    @Override
    public List<String> sortAlphabetically() throws IOException {
        StringLinesLogic al = new StringLinesLogic();
        Collections.sort(al.separatedWords());
        return al.words;
    }

    @Override
    public List<String> consonantalSorting() throws IOException {
        StringLinesLogic cons = new StringLinesLogic();
        for (String word : cons.separatedWords()) {
            if (word.length() >= 3 && word.startsWith("a") || word.length() >= 3 && word.startsWith("e")
                    || word.length() >= 3 && word.startsWith("i") ||
                    word.length() >= 3 && word.startsWith("o") || word.length() >= 3 && word.startsWith("u")
                    || word.length() >= 3 && word.toLowerCase().startsWith("y")) {
                uniqwords.add(word);
            }
            Comparator<String> comparator = (s1, s2) -> s1.charAt(1) - s2.charAt(1);
            Collections.sort(uniqwords, comparator);
        }
        return uniqwords;
    }

    @Override
    public List<String> deleteWords() throws IOException {
        StringLinesLogic cons = new StringLinesLogic();
        for (String word : cons.separatedWords()) {
            if (word.startsWith("a") || word.startsWith("e")
                    || word.startsWith("i") || word.startsWith("o") || word.startsWith("u")
                    || word.toLowerCase().startsWith("y")) {
                uniqwords.add(word);
            }
        }
        return uniqwords;
    }

    @Override
    public List<String> cutWords() throws IOException {
        StringLinesLogic cons = new StringLinesLogic();
        for (String word : cons.separatedWords()) {
            if (word.length() > 3) {
                StringBuffer sb = new StringBuffer(word);
                String newcut = sb.substring(1, word.length() - 1);
                uniqwords.add(newcut);
            }
        }
        return uniqwords;
    }

}