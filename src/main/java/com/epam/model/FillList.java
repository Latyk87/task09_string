package com.epam.model;

import java.io.IOException;
import java.util.List;
/**
 * Interface for StringLinesLogic Class, for build sentences.
 * Created by Borys Latyk on 13/11/2019.
 *
 * @version 2.1
 * @since 10.11.2019
 */
public interface FillList {
    List<String> seperateSentences() throws IOException;
}
