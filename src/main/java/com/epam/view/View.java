package com.epam.view;

import com.epam.Application;
import com.epam.controller.Controller;
import com.epam.controller.ControllerIml;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;
/**
 * Class to connect with controller, this class just show the data.
 *
 * @version 2.1
 * Created by Borys Latyk on 13/11/2019.
 * @since 10.11.2019
 */
public class View {
    private static Logger logger1 = LogManager.getLogger(Application.class);
    private Controller controller;
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    BufferedReader input = new BufferedReader
            (new InputStreamReader(System.in));

    private Locale locale;
private ResourceBundle bundle;

  private  void setMenu(){
      menu = new LinkedHashMap<>();
      menu.put("1", bundle.getString("1"));
      menu.put("2", bundle.getString("2"));
      menu.put("3", bundle.getString("3"));
      menu.put("4", bundle.getString("4"));
      menu.put("5", bundle.getString("5"));
      menu.put("6", bundle.getString("6"));
      menu.put("7", bundle.getString("7"));
      menu.put("8", bundle.getString("8"));
      menu.put("9", bundle.getString("9"));
      menu.put("10", bundle.getString("10"));
      menu.put("11", bundle.getString("11"));
      menu.put("12", bundle.getString("12"));
      menu.put("Q", bundle.getString("Q"));
  }
public View() {
        locale = new Locale("en");
        bundle = ResourceBundle.getBundle("String",locale);
        controller = new ControllerIml();
        setMenu();
        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::pressButton1);
        methodsMenu.put("2", this::pressButton2);
        methodsMenu.put("3", this::pressButton3);
        methodsMenu.put("4", this::pressButton4);
        methodsMenu.put("5", this::pressButton5);
        methodsMenu.put("6", this::pressButton6);
        methodsMenu.put("7", this::pressButton7);
        methodsMenu.put("8", this::pressButton8);
        methodsMenu.put("9", this::pressButton9);
        methodsMenu.put("10", this::pressButton10);
        methodsMenu.put("11", this::pressButton11);
        methodsMenu.put("12", this::pressButton12);
        methodsMenu.put("Q", this::pressButton13);
    }

    private void pressButton1() throws IOException {
    logger1.info(controller.lines());
  }

    private void pressButton2() throws IOException {

        logger1.info(controller.sentencesLenght());
    }

    private void pressButton3() throws IOException {
        logger1.info(controller.uniqueWord());
    }

    private void pressButton4() throws IOException {
        logger1.info(controller.questionSentences());
    }

    private void pressButton5() throws IOException {
        logger1.info(controller.changeWords());
    }

    private void pressButton6() throws IOException {
        logger1.info(controller.sortAlphabetically());
    }
    private void pressButton7() throws IOException {
        logger1.info(controller.consonantalSorting());
    }
    private void pressButton8() throws IOException {
        logger1.info(controller.deleteWords());
    }
    private void pressButton9() throws IOException {
        logger1.info(controller.cutWords());
    }

    private void pressButton10() throws Exception {
        locale = new Locale("uk");
        bundle = ResourceBundle.getBundle("String",locale);
        setMenu();
        show();
    }
    private void pressButton11() throws Exception {
        locale = new Locale("en");
        bundle = ResourceBundle.getBundle("String",locale);
        setMenu();
        show();
    }
    private void pressButton12() throws Exception {
        locale = new Locale("da");
        bundle = ResourceBundle.getBundle("String",locale);
        setMenu();
        show();
    }
    private void pressButton13() {
        logger1.info("Bye-Bye");
    }
    //-------------------------------------------------------------------------

    private void outputMenu() {
        logger1.info("\nBig Task:");
        for (String str : menu.values()) {
            System.out.println(str);
        }
    }

    public void show() throws Exception {
        String keyMenu;
        do {
            outputMenu();
            logger1.info("Please, select menu point");
            keyMenu = input.readLine().toUpperCase();
            methodsMenu.get(keyMenu).print();
        } while (!keyMenu.equals("Q"));
    }
}


