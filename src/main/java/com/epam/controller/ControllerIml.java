package com.epam.controller;

import com.epam.model.Model;
import com.epam.model.StringLinesLogic;

import java.io.*;
import java.util.List;
import java.util.Set;


/**
 * Class contains methods for proper working of controller.
 * Created by Borys Latyk on 13/11/2019.
 *
 * @version 2.1
 * @since 10.11.2019
 */
public class ControllerIml implements Controller {

    private Model link;

    @Override
    public List<String> lines() throws IOException {
       link = new StringLinesLogic(new ControllerIml());
        return link.lines();
    }

    @Override
    public List<String> sentencesLenght() throws IOException {
        link = new StringLinesLogic(new ControllerIml());
        return link.sentencesLenght();
    }

    @Override
    public List<String> separatedWords() throws IOException {
        link = new StringLinesLogic(new ControllerIml());
        return link.separatedWords();
    }

    @Override
    public List<String> uniqueWord() throws IOException {
        link = new StringLinesLogic(new ControllerIml());
        return link.uniqueWord();
    }

    @Override
    public Set<String> questionSentences() throws IOException {
        link = new StringLinesLogic(new ControllerIml());
        return link.questionSentences();
    }

    @Override
    public List<String> changeWords() throws IOException {
        link = new StringLinesLogic(new ControllerIml());
        return link.changeWords();
    }

    @Override
    public List<String> sortAlphabetically() throws IOException {
        link = new StringLinesLogic(new ControllerIml());
        return link.sortAlphabetically();
    }

    @Override
    public List<String> consonantalSorting() throws IOException {
        link = new StringLinesLogic(new ControllerIml());
        return link.consonantalSorting();
    }

    @Override
    public List<String> deleteWords() throws IOException {
        link = new StringLinesLogic(new ControllerIml());
        return link.deleteWords();
    }

    @Override
    public List<String> cutWords() throws IOException {
        link = new StringLinesLogic(new ControllerIml());
        return link.cutWords();
    }


}
